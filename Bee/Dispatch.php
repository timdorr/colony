<?php
/**
 * Colony
 * Copyright (c) Army of Bees (www.armyofbees.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @category   Colony
 * @package    Bee
 * @copyright  Copyright (c) Army of Bees (www.armyofbees.com)
 * @license    http://www.opensource.org/licenses/mit-license.php MIT License
 */

/* Catch errors as exceptions */
set_error_handler(create_function('$x, $y', 'throw new Exception($y, $x);'), E_ALL & ~E_NOTICE & ~E_STRICT);

/**
 * @see Bee_Exception
 */
require_once 'Bee/Exception.php';

/**
 * @see Bee_Registry
 */
require_once 'Bee/Registry.php';

/**
 * @see Bee_Input
 */
require_once 'Bee/Input.php';

/**
 * @see Bee_Display
 */
require_once 'Bee/Display.php';

/**
 * @see Net_URL_Mapper
 */
require_once 'Net/URL/Mapper.php';

/**
 * Dispatcher to route HTTP requests into controllers and back out to a display
 * adapter.
 *
 * @category   Colony
 * @package    Bee
 * @copyright  Copyright (c) Army of Bees (www.armyofbees.com)
 * @license    http://www.opensource.org/licenses/mit-license.php MIT License
 */
class Bee_Dispatch
{
    /**
     * Application config
     * @var array
     */
    public $config = array();

    /**
     * Base URL
     * @var string
     */
    protected $_baseURL = null;

    /**
     * Determines if exceptions should escape the Bee_Core object or be released to
     * the method caller.
     * @var boolean
     */
    protected $_throwExceptions = false;

    /**
     * Determines if exceptions should be logged.
     * @var boolean
     */
    protected $_logExceptions = false;

    /**
     * Determines if exceptions should be emailed to an administrator.
     * @var boolean
     */
    protected $_emailExceptions = false;

    /**
     * Default action if not specified in HTTP request URI
     * @var string
     */
    protected $_defaultAction = 'index';

    /**
     * Namespace from HTTP request URI
     * @var string
     */
    public $namespace = '';

    /**
     * Action from HTTP request URI
     * @var string
     */
    public $action = '';

    /**
     * Method from HTTP request URI
     * @var string
     */
    public $method = '';

    /**
     * Extra data from HTTP request URI
     * @var mixed
     */
    public $extra = null;

    /**
     * The controller object
     * @var Bee_Controller
     */
    private $controller = null;

    /**
     * Constructor
     *
     * @return void
     */
    public function __construct()
    {
        global $CONFIG;
        $conf =& Bee_Registry('config');
        $conf = $this->config =& $CONFIG;

        $this->_setupExceptionHandling();

        $this->_baseURL = preg_replace( '#index\.php.*#i', '', $_SERVER['PHP_SELF'] );

        Bee_Display::factory( $this->config['display_backend'] );
    }

    /**
     * Sets the throwExceptions flag and retreives the current status.
     *
     * By default, exceptions are caught by the Bee_Dispatch class. Enabling
     * this flag will allow them to pass back to the caller.
     *
     * @param boolean $flag
     * @return boolean
     */
    public function throwExceptions( $flag = null )
    {
        if( $flag !== null )
        {
            $this->_throwExceptions = (bool) $flag;
            return $this;
        }

        return $this->_throwExceptions;
    }

    /**
     * Sets the logExceptions flag and retreives the current status.
     *
     * By default, exceptions are caught by the Bee_Dispatch class.
     * Enabling this flag will additionally log them to a file defined
     * by $CONFIG['exception_log'] (in app/Config.php).
     *
     * @param boolean $flag
     * @return boolean
     */
    public function logExceptions( $flag = null )
    {
        if( $flag !== null )
        {
            $this->_logExceptions = (bool) $flag;
            return $this;
        }

        return $this->_logExceptions;
    }

    /**
     * Sets the emailExceptions flag and retreives the current status.
     *
     * By default, exceptions are caught by the Bee_Dispatch class.
     * Enabling this flag will additionally email them to an administrator
     * (or whoever's email is passed in as the 2nd parameter).
     *
     * @param boolean $flag
     * @return boolean
     */
    public function emailExceptions( $flag = null, $emailAddress = 'nobody' )
    {
        if( $flag !== null )
        {
            $this->_emailExceptions = (bool) $flag;
            return $this;
        }

        return $this->_emailExceptions;
    }

    /**
     * Starts execution of the Colony framework.
     *
     * @throws Bee_Exception
     * @return void
     */
    public function run()
    {
        try
        {
            $this->_dispatch();
        }
        catch( Exception $e )
        {
            // Log the exception if we're configured to do that
            if( $this->logExceptions() )
            {
                if(!isset( $this->controller )) {
                    require_once('Bee/Controller.php');
                    call_user_func_array( array( new Bee_Controller($this->config), 'logException'), array($e) );
                } else {
                    call_user_func_array( array( $this->controller, 'logException'), array($e) );
                }
            }
            
            // Email the exception if we're configured to do that
            if( $this->emailExceptions() )
            {
                if(!isset( $this->controller )) {
                    require_once('Bee/Controller.php');
                    call_user_func_array( array( new Bee_Controller($this->config), 'emailException'), array($e) );
                } else {
                    call_user_func_array( array( $this->controller, 'emailException'), array($e) );
                }
            }
             
            // Throw the exception if we're configured to do that
            if( $this->throwExceptions() )
            {
                throw $e;
            }
            else
            {
                if( file_exists( 'app/views/error.tpl' ) )
                {
                    $error_message = preg_replace( '/mysql_connect\([^)]+?\)/im', 'mysql_connect()', $e );
                    Bee_Display::display( 'error', array( 'config' => $this->config,
                                                          'error' => $error_message,
                                                          'exception' => $e ) );
                }
                else
                {
                    print '<pre>';
                    print $e;
                    print '</pre>';
                }
            }
        }
    }

    /**
     * Dispatches an HTTP request to its assigned controller
     *
     * @throws Bee_Dispatch_Exception
     * @return bool
     */
    private function _dispatch()
    {
        $this->_loadRouting();
        $route = $this->_parseRoute();

        // Check that controller class exists
        if( !file_exists( 'app/controllers/' . str_replace( '\\', '/', $this->namespace ) . $this->action . '.php' ) )
        {
            if( $this->throwExceptions() || !file_exists( 'app/views/404.tpl' ) )
            {
                header( 'HTTP/1.1 404 Not Found' );
                throw new Bee_Dispatch_Exception( "Controller ($this->namespace\\$this->action) not found" );
            }
            else
            {
                header( 'HTTP/1.1 404 Not Found' );
                Bee_Display::display( '404' );
                return false;
            }
        }

        // Instantiate the action controller
        require_once 'controllers/' . str_replace( '\\', '/', $this->namespace ) . $this->action . '.php';
        $action = '\\' . ucfirst( $this->namespace ) . ucfirst( $this->action ) . '_Controller';
        $this->controller = $controller = new $action( array_merge( $this->config, array( 'baseURL' => $this->_baseURL, 'route' => $route ) ) );

        // If a method wasn't defined in the URI, grab the default from the controller
        if( $this->method == '' )
            $this->method = $controller->defaultMethod;

        // Check that method call exists
        if( !method_exists( $controller, $this->method ) )
            throw new Bee_Dispatch_Exception( 'Method not found: ' . $action . '::' . $this->method );

        // Save the local vars to the controller
        $controller->namespace = substr( $this->namespace, 0, -1 );
        $controller->action = $this->action;
        $controller->method = $this->method;
        $controller->extra = $this->extra;
        $controller->_baseURL = $this->_baseURL;

        // Run the method
        if ( method_exists( $controller, "_setup" ) )
            $controller->_setup();
        $controller->{$this->method}( $this->extra );
        $controller->completeDispatch();

        // Now let the controller render
        $controller->render( str_replace( '\\', '/', $this->namespace ) . $this->action . '/' . $this->method );

        return true;
    }

    /**
     * Splits the HTTP request URI into action, method, extra data. Applies custom routing.
     *
     * @return array The parsed route
     */
    private function _parseRoute()
    {
        // Get our Net_URL_Mapper instance
        $m = Net_URL_Mapper::getInstance();
        
        // If we have a prefix, set it
        if( $this->_baseURL != '/' )
            $m->setPrefix( $this->_baseURL );
        
        // Apply routing
        if( isset( $this->config['routing'] ) )
            foreach( $this->config['routing'] as $pattern => $mapping )
                $m->connect( $pattern, $mapping );
    
        // Connect our default route
        $m->connect( ':action/:method/*(extra)', array( 'namespace' => '', 'action' => 'index', 'method' => '', 'extra' => null ) );
        
        // Get our route and hook everything up
        $route = $m->match( $_SERVER['REQUEST_URI'] );
        
        // An empty namespace doesn't have a trailing slash
        if( !empty( $route['namespace'] ) )
            $this->namespace = $route['namespace'] . '\\';
        else
            $this->namespace = '';

        $this->action = $route['action'];
        $this->method = $route['method'];

        // Extra isn't required, so it's sometimes not defined
        if( isset( $route['extra'] ) )
            $this->extra = $route['extra'];
        else
            $this->extra = null;
            
        return $route;
    }

    /**
     * Loads routing from app/Routing.php  Note: routes were
     * previously stored in app/Config.php but moved so that
     * they could be versioned.
     *
     * @return void
     */
    private function _loadRouting()
    {
        if( file_exists( './app/Routing.php' ) )
        {
            include( './app/Routing.php' );

            if( isset( $ROUTING ) )
            {
                global $CONFIG;
                $CONFIG['routing'] = $ROUTING;
            }
        }
    }

    private function _setupExceptionHandling()
    {
        // Exception logging
        if( !isset($this->config['log_exceptions']) || $this->config['log_exceptions'] !== false )
        {
            $this->logExceptions( true );
        }

        // Exception emailing
        if( isset($this->config['email_exceptions']) && $this->config['email_exceptions'] )
        {
            $this->emailExceptions( true );
        }

        // Exception throwing
        if( !isset($this->config['throw_exceptions']) || $this->config['throw_exceptions'] )
        {
            $this->throwExceptions( true );
        }
    }

}

class Bee_Dispatch_Exception extends Bee_Exception
{}
