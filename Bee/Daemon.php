<?php
/**
 * Colony
 * Copyright (c) Army of Bees (www.armyofbees.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @category   Colony
 * @package    Bee
 * @copyright  Copyright (c) Army of Bees (www.armyofbees.com)
 * @license    http://www.opensource.org/licenses/mit-license.php MIT License
 */

/**
 * @see Bee_Exception
 */
require_once 'Bee/Exception.php';

/**
 * Job scheduling server. Dishes up job tasks to client systems to run 
 * in a distributed environment.
 *
 * @category   Colony
 * @package    Bee
 * @copyright  Copyright (c) Army of Bees (www.armyofbees.com)
 * @license    http://www.opensource.org/licenses/mit-license.php MIT License
 */
class Bee_Daemon
{
    /**
     * Task class to execute
     * @var Bee_Daemon
     */
    private $task = true; 

    /**
     * State of child execution
     * @var boolean
     */
    private $_running = true;
    
    /**
     * Application config
     * @var array
     */
    public $config = array();

    /**
     * Constructs a new daemon of either a client or server type.
     *
     * @param array $config The configuration to pass to the backend.
     * @throws Bee_Daemon_Exception
     */
	public function __construct( $config = array() )
	{	
        // Verify that backend parameters are in an array.
        if( !is_array( $config ) )
            throw new Bee_Daemon_Exception( 'Backend parameters must be in an array' );
	}
	
	/**
	 * Checks if the server is to run in a single process.
	 *
	 * @return boolean
	 */
    private function _isSingleProcess()
    {
        global $argv;

        if( isset( $argv[1] ) )
            if( $argv[1] == 'multiple' )
                return false;

        return true;
    }

    /**
     * Forks a daemon process to run.
     *
     * @param $function Daemon process to execute
     * @param array $options Any options to pass to the function
     * @throws Bee_Daemon_Exception
     * @return void
     */
    public function daemonize( $function, $options = array() )
    {
        if( $this->_isSingleProcess() )
        {
            $this->_runLoop( $function, $options );
        }
        else
        {
            // Zombie protection
            pcntl_signal( SIGCHLD, SIG_IGN );

            // Holy forkin' shit!
            $pid = pcntl_fork();
        
            // Check the fork for errors
            if( $pid == -1 )
                throw new Bee_Daemon_Exception("unable to pcntl_fork()");
            // Parent
            elseif( $pid )
            {
                print "Job Server (PID: $pid) started...\n";
                exit( 0 );
            }
            // Child
            else
                $this->_runLoop( $function, $options );
        }
    }

    /**
     * Forks a process to run something in the background. Does not loop.
     *
     * @param $function Daemon process to execute
     * @param array $options Any options to pass to the function
     * @throws Bee_Daemon_Exception
     * @return void
     */
       public function fork( $function, $options = array() )
       {
           // Zombie protection
           pcntl_signal( SIGCHLD, SIG_IGN );

           // Holy forkin' shit!
           $pid = pcntl_fork();

           // Check the fork for errors
           if( $pid == -1 )
               throw new Bee_Daemon_Exception("unable to pcntl_fork()");
           // Parent
           elseif( $pid )
           {
               return;
           }
           // Child
           else {
               call_user_func_array( $function, $options );
               exit(0);
           }
       }

    /**
     * Execution loop for the daemonized process.
     *
     * @param $function Daemon process to execute
     * @param array $options Any options to pass to the function
     * @throws Bee_Daemon_Exception
     * @return void
     */
    private function _runLoop( $function, $options = array() )
    {
        $index = 0;
        while( $this->_running )
        {
            // Increment counter
            $index++;

            // Holy forkin' shit!
            $pid = pcntl_fork();
            
            // Check the fork for errors
            if( $pid == -1 )
                throw new Bee_Daemon_Exception("Unable to pcntl_fork()");
            // Parent
            elseif( $pid )
            {
                pcntl_waitpid( $pid, $status );
            }
            // Child
            else
            {
                call_user_func( $function, $index, $options );
                exit( 0 );
            }
            
            // Run every second
            usleep( 1000000 );
        }
    }
}

class Bee_Daemon_Exception extends Bee_Exception
{}